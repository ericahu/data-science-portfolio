# README #

This repository includes a number of projects that I have contributed alone or with team members. Projects mainly focus around data science, data mining and machine learning.

### Projects

- **Adult Data Set** - UCI Machine Learning Repository
	- http://archive.ics.uci.edu/ml/datasets/adult
	- Binary Classification

### How to run my code

- Python 3
- R