# K NEAREST NEIGHBOURS

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from time import time

from sklearn import preprocessing
from sklearn.metrics import confusion_matrix, accuracy_score, f1_score, precision_score, recall_score
from sklearn.utils import shuffle

from sklearn.neighbors import KNeighborsClassifier
import core

# **************************************************

k, w = 50, 'distance'

def KNN(train, test):
	X = train.iloc[:, :-1]		# X is everything excluding last column
	Y = train.iloc[:, -1]		# Y is the last column
	model = KNeighborsClassifier(n_neighbors=k, weights=w)
	model.fit(X, Y)				# Fit/train model on training data
	return model.predict(test.iloc[:, :-1])	# Return predicted of testing data

def plot_accuracies(array1, array2, fname):
	line1, = plt.plot([0]+array1, label='uniform weights')
	line2, = plt.plot([0]+array2, label='distance weights')
	axes = plt.gca()
	axes.set_xlim([1,len(array1)])
	axes.set_ylim(min(array1+array2)-0.02,max(array1+array2)+0.02)
	plt.ylabel('Accuracy')
	plt.xlabel('Neighbours')
	plt.legend(loc=4, borderaxespad=0.)
	plt.savefig('output/{}.svg'.format(fname), format='svg', bbox_inches='tight', dpi=1000)

# **************************************************

if __name__ == '__main__':
	# Represent the algorithm as lambda function, so we can pass it `cross_validate()` easily
	algorithm = lambda train, test: KNN(train, test)

	# READ IN DATA
	start_time = time()
	train_data = core.read_data('adult.data', 0, 0)
	train_labels = train_data.iloc[:,-1]
	train_data = core.encode_data(train_data)
	test_data = core.read_data('adult.test', 1, 0)	# Skip first line `|1x3 Cross validator`
	# Need to remove full-stop in last column of test data
	test_data.iloc[:,-1] = [y[:-1] for y in test_data.iloc[:,-1].values]
	test_data = core.encode_data(test_data)
	# train_data, test_data = core.filter_features(train_data, test_data)
	print('\n'+'#'*58)

	# TESTING THE MODEL ON TRAINING DATA USING CROSS-VALIDATION
	# # See if different neighbourhood sizes and weights affect performance (uncomment to run)
	# accuracies = [[],[]]
	# i, MAX, best = 0, 0.0, 0
	# for weights in ['uniform', 'distance']:
	# 	for neighbours in range(1,150):
	# 		k, w = neighbours, weights
	# 		y_actu, y_pred = core.cross_validate(10, train_data, algorithm)
	# 		print('(%d neighbours, %s weights)' %(k, weights))
	# 		a = accuracy_score(y_actu, y_pred)
	# 		accuracies[i].append(a)
	# 		if a > MAX:
	# 			best = (neighbours, weights)
	# 			MAX = a
	# 	i += 1
	# plot_accuracies(accuracies[0], accuracies[1], 'knn-train-accuracies')
	#
	# # Perform 10-fold CV on the best combination of neighbours+weights
	# k, w = best
	# print('----------------------------------------------------------')
	# print('\nMAX TRAINING ACCURACY = %.3f (%d neighbours, %s weights)' %(MAX, k, w))

	y_actu, y_pred = core.cross_validate(10, train_data, algorithm)
	print('(%d neighbours, %s weights)' %(k, w))
	# Return the labels of encoded data
	le = preprocessing.LabelEncoder().fit(train_labels)
	y_actu = list(le.inverse_transform(y_actu))
	y_pred = list(le.inverse_transform(y_pred))

	# Make sure to change the filename of confusion matrix
	core.evaluate_performance(y_actu, y_pred, 'knn-train-confusion')

# ****************************************************************************************************

	# TESTING THE MODEL ON TESTING DATA
	print('\n'+'#'*58)

	y_pred = KNN(train_data, test_data)
	y_actu = test_data.iloc[:,-1]
	y_actu = list(le.inverse_transform(y_actu))
	y_pred = list(le.inverse_transform(y_pred))

	core.evaluate_performance(y_actu, y_pred, 'knn-test-confusion')	# Confusion matrix
	print('\n'+'#'*58)
	print('\n[DONE] Total time = %.2f seconds' %(time()-start_time))
	print()
