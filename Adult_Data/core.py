# CORE FUNCTIONS
# This file contains reusable functions for tasks such as reading and encoding data
# `import core` to use these functions in other files

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from time import time

from sklearn import preprocessing
from sklearn.metrics import confusion_matrix, accuracy_score, f1_score, precision_score, recall_score
from sklearn.utils import shuffle

# **************************************************

# From data description: http://archive.ics.uci.edu/ml/datasets/adult
COL_NAME = ['age', 'workclass', 'fnlwgt', 'education', 'education-num', 'marital-status', 'occupation', 'relationship', 'race', 'sex', 'capital-gain', 'capital-loss', 'hours-per-week', 'native-country', 'income']

def read_data(filename, skip_r, skip_f):
	print('\nReading %s...' %filename)
	start_time = time()
	data = pd.read_csv('input/{}'.format(filename), header=None, index_col=None, sep=',\s', engine='python', skiprows=skip_r, skipfooter=skip_f)
	data = remove_missing(data)
	data.columns = COL_NAME		# Rename columns
	print('[OK] Read time = %.2f seconds' %(time()-start_time))
	return data

def encode_data(data):
	print('\nEncoding data...')
	start_time = time()
	for col in data.columns:					# For each column
		if data.loc[:, col].dtypes == 'object':		# If data type is categorical
			le = preprocessing.LabelEncoder()			# Create an encoder that fits its values
			le.fit(data.loc[:, col])					# And replace column with encoded values
			data.loc[:, col] = le.transform(data.loc[:, col])
			# data.drop(col, axis=1)
	print('[OK] Encode time = %.2f seconds' %(time()-start_time))
	return data

def remove_missing(data):
	i = 0
	while i < data.shape[0]:
		if '?' in str(data.iloc[i,:].values):
			data = data.drop(i)
		i += 1
	data = data.reset_index(drop=True)
	return data

def filter_features(train, test):
	from sklearn.svm import LinearSVC
	from sklearn.feature_selection import SelectFromModel
	X, Y = train.iloc[:,:-1], train.iloc[:,-1]
	lsvc = LinearSVC(C=0.1, penalty="l1", dual=False).fit(X, Y)
	model = SelectFromModel(lsvc, prefit=True)

	train_new = pd.DataFrame(data=model.transform(X))
	train_new[train.shape[1]] = train.iloc[:,-1]
	test_new = pd.DataFrame(model.transform(test.iloc[:,:-1]))
	test_new[test.shape[1]] = test.iloc[:,-1]
	return train_new, test_new

def cross_validate(k, data, algorithm):
	y_actu, y_pred = [], []
	print('\nCross validating...')
	start_time = time()
	k_size = data.shape[0] // k		# Size of each cross-validation block

	for i in range(k):
		data = shuffle(data)							# Shuffle data for C
		cv_test = data.iloc[:k_size,]					# Top 1/k as testing
		cv_train = data.iloc[k_size:,]					# Bottom (k-1)/k as training
		y_pred += list(algorithm(cv_train, cv_test))	# Get predicted values from `algorithm()`
		y_actu += list(cv_test.iloc[:, -1])				# Actual values are in last column of data
	print('[OK] CV time = %.2f seconds' %(time()-start_time))
	return y_actu, y_pred

def evaluate_performance(y_actu, y_pred, confusion_matrix_fname):
	start_time = time()
	print('\nEvaluating performance...')
	print('----------------------------------------------------------')
	print('                    Precision      Recall         F-Score')
	print('Macro-average       %.3f          %.3f          %.3f' %(precision_score(y_actu, y_pred, average='macro'), recall_score(y_actu, y_pred, average='macro'), f1_score(y_actu, y_pred, average='macro')))
	print('Micro-average       %.3f          %.3f          %.3f' %(precision_score(y_actu, y_pred, average='micro'), recall_score(y_actu, y_pred, average='micro'), f1_score(y_actu, y_pred, average='micro')))
	print('Weighted-average    %.3f          %.3f          %.3f' %(precision_score(y_actu, y_pred, average='weighted'), recall_score(y_actu, y_pred, average='weighted'), f1_score(y_actu, y_pred, average='weighted')))

	print('\nOVERALL ACCURACY = %.3f' %accuracy_score(y_actu, y_pred))
	print('----------------------------------------------------------')

	print('Saving confusion matrix to output/{}.svg\n'.format(confusion_matrix_fname))
	confusion = pd.crosstab(pd.Series(y_actu, name='Actual'), pd.Series(y_pred, name='Predicted'))
	plot_confusion_matrix(confusion)
	# Save graph into a svg file
	plt.savefig('output/{}.svg'.format(confusion_matrix_fname), format='svg', bbox_inches='tight', dpi=1000)

def plot_confusion_matrix(confusion, title='Confusion Matrix', cmap=plt.cm.gray_r):
	plt.matshow(confusion, cmap=cmap)
	plt.colorbar()
	tick_marks = np.arange(len(confusion.columns))
	plt.xticks(tick_marks, confusion.columns, rotation=0, fontsize=10)
	plt.yticks(tick_marks, confusion.index, fontsize=10)
	plt.ylabel(confusion.index.name, fontsize=11)
	plt.xlabel(confusion.columns.name, fontsize=11)
